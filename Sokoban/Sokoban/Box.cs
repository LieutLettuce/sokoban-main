﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Sokoban
{
    class Box : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;
        // ------------------
        // Behaviour
        // ------------------

        public Box(Texture2D newTexture, Level newLevel)
           : base(newTexture)
        {
            ourLevel = newLevel;
        }
        // ----------------
        public bool TryPush(Vector2 direction)
        {
            // New Position the box will be in after the push
            Vector2 newGridPos = GetTilePosition() + direction;

            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

            //If the target tile is in the wall, we cant move there - return false
            if (tileInDirection != null && tileInDirection is Wall)
            {
                // PLAY SFX Bump
                return false;
            }

            //If the target tile is in the wall, we cant move there - return false
            if (tileInDirection != null && tileInDirection is Box)
            {
                return false;
            }

            // Move our tile (box) to the new position
            return ourLevel.TryMoveTile(this, newGridPos);
        } 
        // ----------------

    }
}
