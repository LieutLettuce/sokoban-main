﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Sokoban
{
    class Player : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;
        private float timeSinceLastMove;

        private const float MOVE_COOLDOWN = 0.5f;

        // ------------------
        // Behaviour
        // ------------------
        public Player(Texture2D newTexture, Level newLevel)
           : base(newTexture)
        {
            ourLevel = newLevel;
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            // Add to Time since we last moved
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timeSinceLastMove += frameTime;


            //Get current keyboard input
            KeyboardState keyboardState = Keyboard.GetState();

            // Check specific keys and record movement
            Vector2 movementInput = Vector2.Zero;

            //Check each key
            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
            }

            // If we have pressed any direction, try and move there!
            if (movementInput != Vector2.Zero && timeSinceLastMove >= MOVE_COOLDOWN)
            {
                TryMove(movementInput);
                timeSinceLastMove = 0;
            }
            
        }
        // ----------------
        private bool TryMove(Vector2 direction)
        {
            Vector2 newGridPos = GetTilePosition() + direction;

            // Ask level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);
            
            //If the target tile is in the wall, we cant move there - return false
            if (tileInDirection != null && tileInDirection is Wall)
            {
                //TODO: PLAY SFX BUMP
                return false;
            }

            //If the target tile is a box try to push it
            if (tileInDirection != null && tileInDirection is Box)
            {
                Box targetBox = tileInDirection as Box;
                bool pushSuccess = targetBox.TryPush(direction);

                if (pushSuccess == false)
                {
                    return false;
                }
            }


            // Ask the level if we can move to this new position.
            // See if the move was successful
            bool moveResult = ourLevel.TryMoveTile(this, newGridPos);

            //return true or false based on move successfullness
            return moveResult;

        }
        // ----------------

    }
}
