﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;
using System.IO;


namespace Sokoban
{
    class Level
    {
        // ------------------
        // Data
        // ------------------
        private Tile[,] tiles;
        private int currentLevel;

        //Assets
        Texture2D wallSprite;
        Texture2D playerSprite;
        Texture2D boxSprite;

        // ------------------
        // Behaviour
        // ------------------
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            wallSprite = content.Load<Texture2D>("graphics/PlayerAnimation");
            wallSprite = content.Load<Texture2D>("graphics/Wall");
            playerSprite = content.Load<Texture2D>("graphics/PlayerStatic");
            boxSprite = content.Load<Texture2D>("graphics/Box");



            // TEMP - this will be moved later
            LoadLevel(1);
        }
        // ------------------
        public void LoadLevel(int levelNum)
        {
            currentLevel = levelNum;
            string baseLevelName = "level_";
            LoadLevel(baseLevelName + levelNum.ToString() + ".txt");
        }
        // ------------------
        public void LoadLevel(string fileName)
        {

            // clear any existing level data 
            ClearLevel();

            // Create filestream to open the file and get it ready for reading
            Stream fileStream = TitleContainer.OpenStream(fileName);

            // Before we read in the individual tiles in the level, we need to know 
            // how big the level is overall to create the arrays to hold the data
            int lineWidth = 0; // Eventually will be levelWidth
            int numLines = 0;  // Eventually will be levelHeight
            List<string> lines = new List<string>();    // this will contain all the strings of text in the file
            StreamReader reader = new StreamReader(fileStream); // This will let us read each line from the file
            string line = reader.ReadLine(); // Get the first line
            lineWidth = line.Length; // Assume the overall line width is the same as the length of the first line
            while (line != null) // For as long as line exists, do something
            {
                lines.Add(line); // Add the current line to the list
                if (line.Length != lineWidth)
                {
                    // This means our lines are different sizes and that is a big problem
                    throw new Exception("Lines are different widths - error occured on line " + lines.Count);
                }

                // Read the next line to get ready for the next step in the loop
                line = reader.ReadLine();
            }

            // We have read in all the lines of the file into our lines list
            // We can now know how many lines there were
            numLines = lines.Count;

            // Now we can set up our tile array
            tiles = new Tile[lineWidth, numLines];

            // Loop over every tile position and check the letter
            // there and load a tile based on  that letter
            for (int y = 0; y < numLines; ++y)
            {
                for (int x = 0; x < lineWidth; ++x)
                {
                    // Load each tile
                    char tileType = lines[y][x];
                    // Load the tile
                    LoadTile(tileType, x, y);
                }
            }
        }
        // ------------------
        private void LoadTile(char tileType, int tileX, int tileY)
        {
            switch (tileType)
            {
                
                // Wall
                case 'W':
                    CreateWall(tileX, tileY);
                    break;

                // Player
                case 'P':
                    CreatePlayer(tileX, tileY);
                    break;

                // Player
                case 'B':
                    CreateBox(tileX, tileY);
                    break;

                // Blank space
                case '.':
                    break; // Do nothing

                // Any non-handled symbol
                default:
                    throw new NotSupportedException("Level contained unsupported symbol " + tileType + " at line " + tileY + " and character " + tileX);
            }
        }
        // ------------------
        private void ClearLevel()
        {
            // TODO:    
        }
        // ------------------
        private void CreateWall(int tileX, int tileY)
        {
            Wall wall = new Wall(wallSprite);
            wall.SetTilePosition(new Vector2 (tileX, tileY));
            tiles[tileX, tileY] = wall;
        }
        // ------------------
        private void CreatePlayer(int tileX, int tileY)
        {
            Player player = new Player(playerSprite, this);
            player.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = player;
        }
        // ------------------
        private void CreateBox(int tileX, int tileY)
        {
            Box box = new Box(boxSprite, this);
            box.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = box;
        }
        // ------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach(Tile tile in tiles)
            {
                if (tile != null)
                {
                    tile.Draw(spriteBatch);
                }
            }
        } // ----------------
        public void Update(GameTime gameTime)
        {
            foreach (Tile tile in tiles)
            {
                if (tile != null)
                {
                    tile.Update(gameTime);
                }
            }
        } // ----------------
        public bool TryMoveTile(Tile toMove, Vector2 newPosition)
        {
            //Get current tile position
            Vector2 currentTilePosition = toMove.GetTilePosition();

            //Check if the new position is in bounds
            int newPosX = (int)newPosition.X;
            int newPosY = (int)newPosition.Y;
            if (newPosX >= 0 && newPosY >= 0 && newPosX < tiles.GetLength(0) && newPosY <tiles.GetLength(1))
            {
                // YES OUR NEW POSIITON IS LEGAL

                // So lets actually move it.

                toMove.SetTilePosition(newPosition);

                // Move it to the correct place in the array, remove it from the old place
                tiles[newPosX, newPosY] = toMove;

                //Remove it
                tiles[(int)currentTilePosition.X, (int)currentTilePosition.Y] = null;

                //We did move it, so return true
                return true;

            }
            else
            {
                // No our new position is out of bounds
                //we did not move it, so return false
                return false;
            }
        }
        // ----------------
        public Tile GetTileAtPosition(Vector2 tilePos)
        {
            //Check if the new position is in bounds
            int PosX = (int)tilePos.X;
            int PosY = (int)tilePos.Y;
            if (PosX >= 0 && PosY >= 0 && PosX < tiles.GetLength(0) && PosY < tiles.GetLength(1))
            {
                // Yes, this coordinate is legal
                return tiles[PosX, PosY];
            }
            else
            {
                //NO, this coordinate is not legal (out of bounds of array /tile grid.
                return null;
            }
        }
        // ----------------
    }
}
