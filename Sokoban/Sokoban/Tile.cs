﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Sokoban
{
    class Tile : Sprite
    {
        // ----------------
        // Data
        // ----------------

        private Vector2 tilePosition;

        private const int TILE_SIZE = 100;
        // ----------------

        // ----------------
        // Behvaiour
        // ----------------

        public Tile(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ----------------
        public void SetTilePosition(Vector2 newTilePosition)
        {
            tilePosition = newTilePosition;
            //Set our position based on tile position
            // multiply our tile position by the tile size
            SetPosition(tilePosition * TILE_SIZE);
        }
        // ----------------
        public Vector2 GetTilePosition() // GETTER LEARN
        {
            return tilePosition;
        }
        // ----------------
        public virtual void Update(GameTime gameTime)
        {
            // Blank - TO be implemented by derived/child classes
        }
        // ----------------

    }
}
